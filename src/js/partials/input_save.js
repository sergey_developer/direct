jQuery(document).ready( function () {


    $('input[type="checkbox"]').each(function (i, e) {
        var id = $(this).attr('id');
        if (localStorage.getItem( id ) == 'true') {
            $(this).attr('checked', 'checked');
        }
        $(this).on('click', function () {
            if ($(this).prop("checked")){
                localStorage.setItem( id , 'true');
            } else {
                localStorage.setItem( id , 'false');
            }
        });
    });

    if (localStorage.getItem('IRadioBuses')) {
        var IRadioBuses = localStorage.getItem('IRadioBuses');
        console.log(IRadioBuses);
        $('input[name="buses"][value="' + IRadioBuses + '"]').attr('checked', 'checked');
    }

    $('input[name="buses"]').each(function (i, e) {
        $(this).on('click', function () {
            localStorage.setItem('IRadioBuses', $(this).val());
        });
    });



})