<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sdk extends Model
{
    protected $table = 'sdks';
    
    public function apps()
    {
        return $this->hasMany('App\App');
    }

    public function bus()
    {
        return $this->belongsTo('App\Bus');
    }

    public function oss()
    {
        return $this->belongsToMany('App\Os', 'os_sdk');
    }

    public function categories()
    {
        return $this->belongsToMany('App\Category', 'categories_sdk');
    }
}
