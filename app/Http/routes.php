<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', 'MainController@index');
Route::post('/', 'MainController@index');
Route::get('/sdk/{sdk_name}', 'MainController@sdk');
Route::get('/admin', 'AdminController@index');
Route::get('/excel', 'ExcelController@index');
Route::get('/images', 'MainController@images');
Route::get('/imageshave', 'MainController@imageshave');
Route::get('/sdk_list', 'MainController@sdkList');
//get('/', ['as' => 'Main', 'uses' => 'MainController@index']);

Route::get('auth/login', 'Auth\AuthController@getLogin');
Route::post('auth/login', 'Auth\AuthController@postLogin');
Route::get('auth/logout', 'Auth\AuthController@getLogout');
Route::get('auth/register', 'Auth\AuthController@getRegister');
Route::post('auth/register', 'Auth\AuthController@postRegister');

//Route::group(['prefix'=>'adminzone','middleware'=>'auth'], function()
//{
//    Route::get('/', function()
//    {
//        echo "Hello admin!";
//    });
//});