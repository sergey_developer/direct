<?php

namespace App\Http\Controllers;

use App\Models\Sdk\App;
use App\Models\Sdk\Bus;
use App\Models\Sdk\Category;
use App\Models\Sdk\Download;
use App\Models\Sdk\Image;
use App\Models\Sdk\Os;
use App\Models\Sdk\Sdk;
use App\Models\Setting;
use Illuminate\Http\Request;
use Illuminate\Http\Response;


use App\Http\Requests;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;

class MainController extends Controller
{

    public function sdk(Request $request, $sdk_name) {

        $sdk = Sdk::where('sdk_link', $sdk_name)->first();

//        var_dump($sdk);

        if ($sdk instanceof Sdk) {
            return view('main.sdk');
        } else {
            abort(404);
        }



    }
    
    public function index(Request $request) {


        $sdks = Sdk::select()->whereIn('id', explode(',', Setting::byName('main_sdks_id_array')->value))->get();

//        dd($sdks);

        $data = [
            'sdks' => $sdks,
        ];


        return view('main.mainnew', $data);
    }

    public function sdkList(Request $request) {


        $sdks = Sdk::queryStart();
//        $sdks = Sdk::with('oss');
//        $buses = Input::get('buses', false);


        if (isset($_POST['buses']) && $_POST['buses'] != 'all') {
            $sdks = $sdks->widthBus($_POST['buses']);
        }
        if (isset($_POST['oss']) && count($_POST['oss']) != 0) {
            $sdks = $sdks->widthOss($_POST['oss']);
        }
        if (isset($_POST['categories'])) {
            $sdks = $sdks->widthCategories($_POST['categories']);
        }


        $sdks = $sdks->get();




        $data = [
            'sdks' => $sdks,
            'buses' => Bus::all(),
            'categories' => Category::all(),
            'oss' => Os::all(),
            'poster' => json_encode($_POST),
        ];

        return view('main.main', $data);
    }
    
    public function images() {
        $sdks = Sdk::all();

        $data = [
            'sdks' => $sdks,
        ];

        return view('main.images', $data);
    }

    public function imageshave() {
        $ids = DB::select('Select `sdk_id` FROM `documentations` GROUP BY `sdk_id`');
        $id_array = array();
        foreach ($ids as $id) {
           array_push($id_array, $id->sdk_id);
        }

        $sdks = Sdk::select()->whereNotIn('id',$id_array)->get();



        dd($sdks);

        $images = Image::all();

        $data = [
            'images' => $images,
        ];

        return view('main.imageshave', $data);


    }
}
