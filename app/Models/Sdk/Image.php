<?php

namespace App\Models\Sdk;

use Illuminate\Database\Eloquent\Model;
use App\MainModel;


class Image extends MainModel
{
    protected $table = 'images';

    protected $fillable = ['link'];


    public function sdks()
    {
        return $this->belongsTo('App\Models\Sdk\Sdk');
    }
}

