<?php

namespace App\Models\Sdk;

use Illuminate\Database\Eloquent\Model;
use App\MainModel;


class Os extends MainModel
{
    protected $table = 'oss';

    protected $fillable = ['name'];

    public function sdks()
    {
        return $this->belongsToMany('App\Models\Sdk\Sdk', 'os_sdk');
    }
}
