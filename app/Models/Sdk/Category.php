<?php

namespace App\Models\Sdk;

use Illuminate\Database\Eloquent\Model;
use App\MainModel;


class Category extends MainModel
{
    protected $table = 'categories';

    protected $fillable = ['name'];


    public function categories()
    {
        return $this->belongsToMany('App\Models\Sdk\Sdk', 'categories_sdk');
    }

    public function scopeGetIdByName($query, $name) {
        return Category::where('name', $name)->first()->id;
    }
}
