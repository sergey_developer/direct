<?php

namespace App\Models\Sdk;

use Illuminate\Database\Eloquent\Model;
use App\MainModel;


class Bus extends MainModel
{
    protected $table = 'buses';

    protected $fillable = ['name'];

    public function sdks()
    {
        return $this->hasMany('App\Models\Sdk\Sdk');
    }

    public function scopeFindByName($request, $name)
    {
        return Bus::where('name', $name)->first();
    }

}
