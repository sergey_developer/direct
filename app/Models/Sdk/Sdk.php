<?php

namespace App\Models\Sdk;

use App\MainModel;
use Illuminate\Database\Eloquent\Model;


class Sdk extends MainModel
{
    protected $table = 'sdks';

    protected $guarded = array();

//    public function apps()
//    {
//        return $this->hasMany('App\Models\App');
//    }

    public function images()
    {
        return $this->hasMany('App\Models\Sdk\Image');
    }

    public function downloads()
    {
        return $this->hasMany('App\Models\Sdk\Download');
    }

    public function documentations()
    {
        return $this->hasMany('App\Models\Sdk\Documentation');
    }

    public function bus()
    {
        return $this->belongsTo('App\Models\Sdk\Bus');
    }

    public function oss()
    {
        return $this->belongsToMany('App\Models\Sdk\Os', 'os_sdk', 'sdk_id', 'os_id');
    }

    public function categories()
    {
        return $this->belongsToMany('App\Models\Sdk\Category', 'categories_sdk');
    }

    public static function queryStart()
    {
        return Sdk::on();
    }

    public function scopeFindByName($request, $name)
    {
        return $request->where('name', $name)->first();
    }

    public function scopeWidthBus($request, $bus) {
        return $request->whereHas('bus', function ($query) use ($bus){
            $query->where('name', $bus);
        });
    }

    public function scopeWidthOss($request, $oss) {
        $request = $request->with('oss');
        foreach ($oss as $os) {
            $request->whereHas('oss', function ($query) use ($os)
            {
                $query->where('name', '=', $os);
            });
        }
    }

    public function scopeWidthCategories($request, $categories) {
        $request = $request->with('categories');
        foreach ($categories as $category) {
            $request->whereHas('categories', function ($query) use ($category)
            {
                $query->where('name', '=', $category);
            });
        }
    }

}
