<?php

namespace App\Models\Sdk;

use Illuminate\Database\Eloquent\Model;
use App\MainModel;
use Illuminate\Support\Facades\DB;

class Download extends MainModel
{
    protected $table = 'downloads';

    protected $fillable = ['name_os', 'version', 'link'];


    public function sdks()
    {
        return $this->belongsTo('App\Models\Sdk\Sdk');
    }

    public function scopeListPossibilityOs() {
        return DB::table('downloads')->select('name_os')->groupBy('name_os')->get();
    }
}
