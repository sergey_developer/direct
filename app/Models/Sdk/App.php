<?php

namespace App\Models\Sdk;

use Illuminate\Database\Eloquent\Model;
use App\MainModel;

class App extends MainModel
{
    protected $table = 'apps';

    protected $fillable = ['name', 'company_name', 'icon', 'sdk_id'];

    public function sdks()
    {
        return $this->belongsTo('App\Models\Sdk\Sdk');
    }
}
