<?php

namespace App\Models\Sdk;

use Illuminate\Database\Eloquent\Model;
use App\MainModel;
use Illuminate\Support\Facades\DB;

class Documentation extends MainModel
{
    protected $table = 'documentations';

    protected $fillable = ['link'];


    public function sdks()
    {
        return $this->belongsTo('App\Models\Sdk\Sdk');
    }

}
