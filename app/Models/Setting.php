<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\MainModel;


class Setting extends MainModel
{
    protected $table = 'settings';

    protected $fillable = ['name', 'value', 'help'];

    public function scopeByName($request, $name) {
        return $request->where('name', $name)->first();
    }
}
