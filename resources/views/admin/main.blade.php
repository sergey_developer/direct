@extends('layouts.direct')

@section('content')


<div class="content" class="col-xs-8 col-xs-offset-2">
    <div class="container">
        <div class="preloader"></div>
    </div>
</div>

<div style="display: none;" id="template_sdk" >
    <div class="row">
        <div class="col-xs-3">name:</div>
        <div class="col-xs-9" name="sdk_name"></div>
    </div>
    <div class="row">
        <div class="col-xs-3">company_name:</div>
        <div class="col-xs-9" name="sdk_company_name"></div>
    </div>
</div>
<style>
</style>

<?php $i=1;?>

<script>
    sdks = <?=$sdks->toJson()?>;
</script>

@endsection