@extends('layouts.direct')

@section('content')


    <div class="row">
        <form id="params" method="post" action="/"  class="col-xs-2 params-sdk">
            <input type="hidden" name="_token" value="<?php echo csrf_token() ?>">
            <div class="inputs os-list row">
                <?php foreach ($oss as $os): ?>
                <div class="row">
                    <div class="col-xs-3">
                    <?php echo $os->name ?>
                    </div>
                    <div class="col-xs-3 col-xs-offset-1">
                        <input id="os<?php echo $os->id?>" type="checkbox" name="oss[]" value="<?php echo $os->name ?>">
                    </div>
                </div>
                <?php endforeach;?>
            </div>
            <div class="inputs categories-list row">
                <?php foreach ($categories as $category) {?>
                <div class="row">
                    <div class="col-xs-3">
                        <?php echo $category->name?>
                    </div>
                    <div class="col-xs-3 col-xs-offset-1">
                        <input id="category<?php echo $category->id?>" type="checkbox" name="categories[]" value="<?php echo$category->name?>">
                    </div>
                </div>
                <?php }?>
            </div>
            <div class="inputs business-list row">
                <?php foreach ($buses as $bus) {?>
                <div class="row">
                    <div class="col-xs-3">
                        <?php echo $bus->name?>
                    </div>
                    <div class="col-xs-3 col-xs-offset-1">
                        <input type="radio" name="buses" value="<?php echo $bus->name?>">
                    </div>
                </div>
                <?php }?>
                <div class="row">
                    <div class="col-xs-3">
                        all
                    </div>
                    <div class="col-xs-3 col-xs-offset-1">
                        <input type="radio" name="buses" value="all">
                    </div>
                </div>
            </div>
            <div>
                <button type="submit" value="submit">Find</button>
            </div>
        </form>
        <div class="col-xs-9 sdk-list">
            <?php $i=0;?>
                <div class="row">
            <?php foreach ($sdks as $sdk): ?>
                    <?php if (($i % 3) == 0 && $i != 0) {?>
                    <?php echo "<div class='row'></div>"; }?>
                <div class="col-xs-4 sdk-item">
                    <div class="col-xs-4">
                        <div class="col-xs-6">
                            name:
                        </div>
                        <div class="col-xs-6">
                            <?php echo $sdk->name?>
                        </div>
                        <div class="col-xs-6">
                            company_name:
                        </div>
                        <div class="col-xs-6">
                            <?php echo $sdk->company_name?>
                        </div>
                    </div>
                    <div class="col-xs-4">
                        <div class="col-xs-12">
                            <a href="<?php echo $sdk->name?>">Company url</a>
                        </div>
                        <div class="col-xs-12">
                            <a href="<?php echo $sdk->company_name?>">Download url</a>
                        </div>
                    </div>
                    <div class="col-xs-4">
                        <div class="col-xs-12">
                            <?php $sdk_oss=""?>
                            <?php foreach ( $sdk->oss as $os): ?>
                                <?php $sdk_oss .= $os->name . ', ' ?>
                            <?php endforeach;?>
                            <?php echo substr($sdk_oss, 0, -2)?>
                        </div>
                        <div class="col-xs-12">
                            <?php $sdk_category=""?>
                            <?php foreach ( $sdk->categories as $category): ?>
                                <?php $sdk_category .= $category->name . ', ' ?>
                            <?php endforeach;?>
                            <?php echo substr($sdk_category, 0, -2)?>
                        </div>
                        <div class="col-xs-12">
                            <?php echo $sdk->bus->name?>
                        </div>
                    </div>
                </div>
                <?php $i++;?>
            <?php endforeach;?>
                </div>
        </div>
    </div>

    <script>
        window.post = <?php echo $poster?>
    </script>
@endsection