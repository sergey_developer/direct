@extends('layouts.direct')

@section('content')

<table>

    <tbody>

    <?php foreach ($sdks as $sdk): ?>

            <tr>
                <td><?php echo $sdk->title?></td>
                <td><?php echo $sdk->description?></td>
                <td><?php echo $sdk->description_formatting?></td>
                <td><?php echo $sdk->company?></td>
                <td><?php echo $sdk->background_image_link?></td>
                <td><?php echo $sdk->of_installs?></td>
                <td><?php echo $sdk->rating_counter?></td>
                <td><?php echo $sdk->categories?></td>
                <td><?php echo $sdk->free_or_paid?></td>
                <td><?php echo $sdk->release_date?></td>
                <td><?php echo $sdk->ios_download_link?></td>
                <td><?php echo $sdk->android_download_link?></td>
                <td><?php echo $sdk->sdk_availability?></td>
                <td><?php echo $sdk->sdk_version_info?></td>
                <td><?php echo $sdk->documentation_links?></td>
                <td><?php echo $sdk->gallery_images?></td>


            </tr>

    <?php endforeach;?>
    </tbody>

</table>

@endsection