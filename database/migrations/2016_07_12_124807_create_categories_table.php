<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categories', function (Blueprint $table) {

            $table->increments('id');

            $table->string('name');

            $table->timestamps();
        });

        Schema::create('categories_sdk', function(Blueprint $table) {

            $table->integer('sdk_id')->unsigned()->index();
            $table->foreign('sdk_id')->references('id')->on('sdks')->onDelete('cascade');

            $table->integer('category_id')->unsigned()->index();
            $table->foreign('category_id')->references('id')->on('categories')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('categories_sdk');
        
        Schema::drop('categories');        
    }
}
