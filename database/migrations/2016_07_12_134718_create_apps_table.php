<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAppsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('apps', function (Blueprint $table) {
            
            $table->increments('id');
            
            $table->string('name');
            $table->string('company_name');
            $table->unique(array('name', 'company_name'));
            
            $table->string('icon');

            $table->integer('sdk_id')->unsigned();
            $table->foreign('sdk_id')->references('id')->on('sdks')->onDelete('cascade');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('apps');
    }
}
