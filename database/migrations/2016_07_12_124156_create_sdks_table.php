<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSdksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sdks', function (Blueprint $table) {

            $table->increments('id');

            $table->string('name');
            $table->string('company_name');            
            $table->unique(array('name', 'company_name'));
            $table->string('sdk_link')->unique();

            $table->date('release');

            $table->integer('bus_id')->unsigned();
            $table->foreign('bus_id')->references('id')->on('buses');

            $table->string('logo');

            $table->string('downloads');

            $table->enum('rank', ['1', '2', '3', '4', '5']);

            $table->text('description');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('sdks');
    }
}
