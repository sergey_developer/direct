<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOssTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('oss', function (Blueprint $table) {

            $table->increments('id');

            $table->string('name');

            $table->timestamps();
        });

        Schema::create('os_sdk', function(Blueprint $table) {

            $table->integer('sdk_id')->unsigned()->index();
            $table->foreign('sdk_id')->references('id')->on('sdks')->onDelete('cascade');

            $table->integer('os_id')->unsigned()->index();
            $table->foreign('os_id')->references('id')->on('oss')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        
        Schema::drop('os_sdk');
        
        Schema::drop('oss');        
    }
}
