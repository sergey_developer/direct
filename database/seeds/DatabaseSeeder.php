<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Models\Bus;
use App\Models\User;
use App\Models\Sdk;
use App\Models\Category;
use App\Models\Os;
use App\Models\App;
use Faker as Fakcers;
use Illuminate\Support\Str;
use Maatwebsite\Excel\Facades\Excel;
use App\Models\Setting;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
//
//        $this->call('UsersSeeder');
//
        $this->call('BusesSeeder');
        $this->call('CategoriesSeeder');
        $this->call('OssSeeder');
        $this->call('SdksSeeder');
//        $this->call('AppSeeder');
    }
}


class CategoriesSeeder extends Seeder {

    public function run() {
        Category::create([ 'name' => 'Framework', ]);
        Category::create([ 'name' => 'Analytics', ]);
        Category::create([ 'name' => 'Performance Monitoring', ]);
        Category::create([ 'name' => 'Cloud', ]);
        Category::create([ 'name' => 'Data Management', ]);
        Category::create([ 'name' => 'Tools', ]);
        Category::create([ 'name' => 'Web', ]);
        Category::create([ 'name' => 'Platform', ]);
        Category::create([ 'name' => 'Communication', ]);
        Category::create([ 'name' => 'Push Notifications', ]);
        Category::create([ 'name' => 'Marketing Automation', ]);
        Category::create([ 'name' => 'Social', ]);
        Category::create([ 'name' => 'Monetization', ]);
        Category::create([ 'name' => 'Big Data', ]);
        Category::create([ 'name' => 'Advertising', ]);
        Category::create([ 'name' => 'Tracking', ]);
        Category::create([ 'name' => 'Engagement', ]);
        Category::create([ 'name' => 'Image Processing', ]);
        Category::create([ 'name' => 'Media', ]);
        Category::create([ 'name' => 'Games', ]);
        Category::create([ 'name' => 'Network', ]);
        Category::create([ 'name' => 'Security', ]);
        Category::create([ 'name' => 'Payment', ]);
        Category::create([ 'name' => 'Graphics', ]);
        Category::create([ 'name' => 'Rewarded Videos', ]);
        Category::create([ 'name' => 'Ad Mediation', ]);
        Category::create([ 'name' => 'Reporting', ]);
        Category::create([ 'name' => 'Crash Reporting', ]);
        Category::create([ 'name' => 'Sport', ]);
        Category::create([ 'name' => 'Wearable', ]);
        Category::create([ 'name' => 'Location', ]);
        Category::create([ 'name' => 'Audio', ]);
        Category::create([ 'name' => 'Testing', ]);
        Category::create([ 'name' => 'Augmented Reality', ]);
        Category::create([ 'name' => 'Rewards', ]);
        Category::create([ 'name' => 'Activity Recognition', ]);
        Category::create([ 'name' => 'In-app support', ]);
        Category::create([ 'name' => 'Loyalty', ]);
        Category::create([ 'name' => 'In-App Feedback', ]);
        Category::create([ 'name' => 'Audience Intelligence', ]);
        Category::create([ 'name' => 'Content Discovery', ]);
    }
}

class BusesSeeder extends Seeder {

    public function run(){

        Bus::create([
            'name' => 'free',
        ]);

        Bus::create([
            'name' => 'freemium',
        ]);

        Bus::create([
            'name' => 'paid',
        ]);
    }
}

class OssSeeder extends Seeder {

    public function run() {

        Os::create([
            'name' => 'iOS',
        ]);

        Os::create([
            'name' => 'Android',
        ]);
    }
}


class SettingsSeeder extends Seeder {

    public function run() {

        Setting::create([
            'name' => 'main_sdks_id_array',
            'value' => '1,2,3,4,5,6',
        ]);
    }
}

class SdksSeeder extends Seeder {

    public function run() {


        $sdk_list = Excel::load('storage/excel/safesd.xls', function($reader)
        {})->toObject()[0];
        foreach ($sdk_list as $sdk) {

            $date = ($sdk->release_date == "N/A") ? "" : $sdk->release_date;


            $new_sdk = new Sdk;
            $new_sdk->name = $sdk->title;
            $new_sdk->company_name = $sdk->company;
            $new_sdk->sdk_link = Str::slug($sdk->title, '-');
            $new_sdk->release = date($date);
            $new_sdk->bus_id = Bus::findByName($sdk->free_or_paid)->id;
            $new_sdk->logo = $sdk->background_image_link;
            $new_sdk->downloads = $sdk->of_installs;
            $new_sdk->rank = $sdk->rating_counter;
            $new_sdk->description = $sdk->description_formatting;

            $new_sdk->save();

            if ($sdk->ios_download_link !== NULL) {
                $new_sdk->downloads()->create([
                    'name_os' => 'iOS',
                    'version' => ($sdk->sdk_version_info_ios == "not supported") ? "" : $sdk->sdk_version_info_ios,
                    'link' => $sdk->ios_download_link,
                ]);
                $new_sdk->oss()->attach([1]);
            }
            if ($sdk->android_download_link !== NULL) {
                $new_sdk->downloads()->create([
                    'name_os' => 'Android',
                    'version' => ($sdk->sdk_version_info_android == "not supported") ? "" : $sdk->sdk_version_info_android,
                    'link' => $sdk->android_download_link,
                ]);
                $new_sdk->oss()->attach([2]);
            }

            for ($i = 1; $i < 23; $i++) {
                $name = 'images' . $i;
                if (isset($sdk->$name) && $sdk->$name != null && $sdk->$name != "") {
                    $new_sdk->images()->create([
                        'link' => $sdk->$name,
                    ]);
                }
            };

            $docs = trim($sdk->documentation_links);
            if (isset($docs) && $docs != "" && $docs != null) {
                $ar_docs = explode(' ', $docs);
                foreach ($ar_docs as $doc) {
                    $doc = trim($doc);
                    if (isset($doc) && $doc != "" && $doc != null) {
                        $new_sdk->documentations()->create([
                            'link' => $doc,
                        ]);
                    }
                }

            }

            $categories = trim($sdk->categories);
            if (isset($docs) && $docs != "" && $docs != null) {
                $categories = explode(',', $categories);
                foreach ($categories as $category) {
                    $category = trim($category);
                    if (isset($category) && $category != "" && $category != null) {
                        $new_sdk->categories()->attach(Category::getIdByName($category));
                    }
                }
            }
        }
    }
}



